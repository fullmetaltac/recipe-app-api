variable "prefix" {
  default = "raa"
}

variable "project" {
  default = "recipe-app-api"
}

variable "contact" {
  default = "malkin.dmytro@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "905418340678.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "905418340678.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}


variable "dns_zone_name" {
  description = "Domain name"
  default     = "beefyduderecipeapp.net"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}

